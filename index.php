<? include_once 'core/core.php';?>
<!doctype html>
<html lang="en">
<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.2.1/css/bootstrap.min.css" integrity="sha384-GJzZqFGwb1QTTN6wy59ffF1BuGJpLSa9DkKMp0DgiMDm4iYMj70gZWKYbI706tWS" crossorigin="anonymous">

    <title>МС Отчеты</title>
</head>
<body>
    <div class="container">
        <ol class="breadcrumb">
            <li class="breadcrumb-item active">Главная</li>
        </ol>

        <h1>Мой Склад Отчеты</h1>

        <div class="list-group col-lg-4">
            <!--<a href="#" class="list-group-item list-group-item-action">Dapibus ac facilisis in</a>-->
            <a class="list-group-item list-group-item-action list-group-item-primary"   href="options/" >Настройки</a>
            <a class="list-group-item list-group-item-action list-group-item-secondary" href="cron/"    >CRON задания</a>
            <a class="list-group-item list-group-item-action list-group-item-success"   href="reports/" >Отчеты</a>
            <a class="list-group-item list-group-item-action list-group-item-danger"    href="data/"    >Данные</a>
            <a class="list-group-item list-group-item-action list-group-item-warning"   href="ms/"      >Мой Склад</a>
            <a class="list-group-item list-group-item-action list-group-item-info"   href="reports/" >Отчеты</a>

            <!--
            <a href="#" class="list-group-item list-group-item-action list-group-item-light">A simple light list group item</a>
            <a href="#" class="list-group-item list-group-item-action list-group-item-dark">A simple dark list group item</a>
            -->
        </div>
    </div>
    <?
//        $sql = "SELECT count(*) FROM ms_demands; ";
//        $data = dbQueryArray($sql);
//        pr($data);
    ?>
    <footer class="footer">
        <div class="container">
            <span class="text-muted">&copy; 2019 kaya.by</span>
        </div>
    </footer>
    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.6/umd/popper.min.js" integrity="sha384-wHAiFfRlMFy6i5SRaxvfOCifBUQy1xHdJ/yoi7FRNXMRBu5WHdZYu1hA6ZOblgut" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.2.1/js/bootstrap.min.js" integrity="sha384-B0UglyR+jN6CkvvICOB2joaf5I4l3gm9GU6Hc1og6Ls7i6U/mkkaduKaBhlAXv9k" crossorigin="anonymous"></script>
</body>
</html>

<?php

    include_once '../core/core.php';

    $data = getDataCount();

    // Получить количество данных
    function getDataCount() {
        $demands = getTableRowsCount('ms_demands');
        $products = getTableRowsCount('ms_products');

        $result = [
            ['name'=>'demands',     'title'=>'Отгрузки',    'amount'=>$demands],
            ['name'=>'products',    'title'=>'Товары',      'amount'=>$products],
        ];
        return $result;

    }

    // Возвращает количество строк в таблице
    function getTableRowsCount($table) {
        $sql = "SELECT count(*) AS amount FROM `$table`; ";
        $data = dbQueryArray($sql);

        $amount = !empty($data[0]['amount']) ? $data[0]['amount'] : 0;
        return $amount;
    }
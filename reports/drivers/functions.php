<?php
    include_once '../../core/core.php';

    $data = getData();

    // Получить количество данных
    function getData() {
        $sql = "
SELECT
	IF(demand_attributes.`value`>'',demand_attributes.`value`, 'Не указан')  AS driver,
	products.`name` AS product,
	products.archived AS archived,
	sum(demand_shipments.quantity) AS amount
FROM
	ms_demands AS demands
	# Водитель
	LEFT JOIN ms_demand_attributes AS demand_attributes ON demand_attributes.demand_uuid = demands.uuid AND demand_attributes.metadataUuid = 'd96aa044-61a8-11e8-9107-504800178379'
	# Товары
	LEFT JOIN ms_demand_shipments  AS demand_shipments  ON demand_shipments.demand_uuid = demands.uuid
	LEFT JOIN ms_products          AS products          ON products.uuid = demand_shipments.product_uuid
WHERE
	1
	AND demands.applicable = 1 
	AND demands.deleted IS NULL
	AND products.`name` > ''
GROUP BY
	demand_attributes.`value`,
	products.`name`
ORDER BY 
	demand_attributes.`value` ASC,
	products.archived ASC,
	products.`name` ASC
;            
        ";
        $data = dbQueryArray($sql);
        return $data;
    }
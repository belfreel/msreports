<? include_once 'functions.php'; ?>
<!doctype html>
<html lang="en">
<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.2.1/css/bootstrap.min.css" integrity="sha384-GJzZqFGwb1QTTN6wy59ffF1BuGJpLSa9DkKMp0DgiMDm4iYMj70gZWKYbI706tWS" crossorigin="anonymous">

    <title>Отчет по водителям - МС Отчеты</title>
</head>
<body>
<div class="container">
    <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="../../">Главная</a></li>
        <li class="breadcrumb-item"><a href="../">Отчеты</a></li>
        <li class="breadcrumb-item active">Водители</li>
    </ol>

    <h1>Отчет по водителям</h1>
    <p>Какой водитель, сколько навозил товара.</p>
    <p>Выбраны проведенные и не удаленные отгрузки. В них просуммировано количество товара в разрезе водителя.</p>
    <table class="table table-striped">
        <thead>
        <tr>
            <th>#</th>
            <th>Водитель &darr;</th>
            <th>Товар &darr;</th>
            <th>Количество</th>
        </tr>
        </thead>
        <tbody>
            <? foreach($data as $k=>$item) { ?>
                <tr class="<?=($item['archived'] == 1 ? 'text-muted' : '')?>">
                    <td><?=($k+1)?></td>
                    <td><nobr><?=$item['driver']?></nobr></td>
                    <td><?=($item['archived'] == 1 ? '<span class="badge badge-secondary" title="Архивный товар">A</span> ' : '')?><?=$item['product']?></td>
                    <td><?=$item['amount']?></td>
                </tr>
            <? } ?>
        </tbody>
    </table>
</div>
<!-- Optional JavaScript -->
<!-- jQuery first, then Popper.js, then Bootstrap JS -->
<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.6/umd/popper.min.js" integrity="sha384-wHAiFfRlMFy6i5SRaxvfOCifBUQy1xHdJ/yoi7FRNXMRBu5WHdZYu1hA6ZOblgut" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.2.1/js/bootstrap.min.js" integrity="sha384-B0UglyR+jN6CkvvICOB2joaf5I4l3gm9GU6Hc1og6Ls7i6U/mkkaduKaBhlAXv9k" crossorigin="anonymous"></script>
</body>
</html>
<?php
    include_once '../../core/core.php';
    include_once '../../moysklad/moysklad.php';

    downloadMS();

    function downloadMS() {
        if (empty($_REQUEST['type'])) {
            return false;
        }
        $type = $_REQUEST['type'];

        $hours = 4800;

        $options = [];
        $options['limit']		= '100';
        //$options['updatedFrom']	= date('Y-m-d%20H:i:s', strtotime(" - $hours hours"));
        //$options['sort']		= 'updated';
        //$options['direction']	= 'asc';
        $options['offset']		= 0;


        // moment
//        $today1 = date('2019-01-01 00:00:00');
//        $today2 = date('2019-02-01 23:59:59');
//        $options['filter'] = urlencode("moment>$today1;moment<$today2");

        // archived
        //$options['filter']		= urlencode("archived=false");

        $stop = 10;

        switch ($type) {
            case 'demands':
                downloadDemands($options);
                break;
            case 'products':
                downloadProducts($options, $stop);
                break;
            default:
                break;
        }
    }
<?php

    include_once '../core/core.php';

    $saved = saveOptions();
    $options = getOptions();

    // Стандартные опции
    function getDefault() {
        return [
            'DB_HOST'       => '',
            'DB_USER'       => '',
            'DB_PASS'       => '',
            'DB_BASE'       => '',
            'MS_LOGIN'      => '',
            'MS_PASSWORD'   => '',
        ];
    }

    // Считывание файла с опциями
    function readOptionsFile() {
        $default = getDefault();

        $file = BASE.OPTIONS_FILE;
        if (!file_exists($file)) {
            return $default;
        }
        $json = file_get_contents($file);
        if (empty($json)) {
            return $default;
        }
        $arr = json_decode($json, true);
        if (empty($arr)) {
            return $default;
        }
        return $arr;
    }

    // Запичь в файл с настройками
    function writeOptionsFile($data) {
        $json = json_encode($data);

        $file = BASE.OPTIONS_FILE;
        file_put_contents($file, $json);

        if (!file_exists($file)) {
            return false;
        }

        $options = readOptionsFile();
        if ($options != $data) {
            return false;
        }

        return true;
    }

    // Получение опций
    function getOptions() {
        $default = getDefault();
        $arr = readOptionsFile();
        $result = [];
        foreach($default as $k=>$v) {
            $result[$k] = !empty($arr[$k]) ? $arr[$k] : '';
        }
        return $result;

    }

    function saveOptions() {
        if (empty($_POST)) {
            return false;
        }

        $options = getDefault();

        foreach($options as $k=>$v) {
            $options[$k] = isset($_REQUEST[$k]) ? $_REQUEST[$k] : $options[$k];
        }

        return writeOptionsFile($options);
    }
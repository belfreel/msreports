<? include_once 'functions.php'; ?>
<!doctype html>
<html lang="en">
<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.2.1/css/bootstrap.min.css" integrity="sha384-GJzZqFGwb1QTTN6wy59ffF1BuGJpLSa9DkKMp0DgiMDm4iYMj70gZWKYbI706tWS" crossorigin="anonymous">

    <title>Настройки - МС Отчеты</title>
</head>
<body>
<div class="container">
    <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="../">Главная</a></li>
        <li class="breadcrumb-item active">Настройки</li>
    </ol>

    <? if ($saved) { ?>
        <div class="alert alert-success" role="alert">
            Настройки сохранены
        </div>
    <? } ?>

    <h1>Настройки</h1>

    <form action="" method="post">
        <h3>База данных</h3>
        <div class="form-group row">
            <label for="DB_HOST" class="col-sm-2 col-form-label">Хост</label>
            <div class="col-sm-3">
                <input id="DB_HOST" name="DB_HOST" value="<?=$options['DB_HOST']?>" placeholder="DB_HOST" class="form-control" />
            </div>
            <label for="DB_HOST" class="col-sm-2 col-form-label">DB_HOST</label>
        </div>
        <div class="form-group row">
            <label for="DB_USER" class="col-sm-2 col-form-label">Пользователь</label>
            <div class="col-sm-3">
                <input id="DB_USER" name="DB_USER" value="<?=$options['DB_USER']?>" placeholder="DB_USER" class="form-control" />
            </div>
            <label for="DB_USER" class="col-sm-2 col-form-label">DB_USER</label>
        </div>
        <div class="form-group row">
            <label for="DB_PASS" class="col-sm-2 col-form-label">Пароль</label>
            <div class="col-sm-3">
                <input id="DB_PASS" name="DB_PASS" value="<?=$options['DB_PASS']?>" placeholder="DB_PASS" class="form-control" />
            </div>
            <label for="DB_PASS" class="col-sm-2 col-form-label">DB_PASS</label>
        </div>
        <div class="form-group row">
            <label for="DB_BASE" class="col-sm-2 col-form-label">База</label>
            <div class="col-sm-3">
                <input id="DB_BASE" name="DB_BASE" value="<?=$options['DB_BASE']?>" placeholder="DB_BASE" class="form-control" />
            </div>
            <label for="DB_BASE" class="col-sm-2 col-form-label">DB_BASE</label>
        </div>
        <h3>Мой Склад </h3>
        <div class="form-group row">
            <label for="MS_LOGIN" class="col-sm-2 col-form-label">Логин</label>
            <div class="col-sm-3">
                <input id="MS_LOGIN" name="MS_LOGIN" value="<?=$options['MS_LOGIN']?>" placeholder="MS_LOGIN" class="form-control" />
            </div>
            <label for="MS_LOGIN" class="col-sm-2 col-form-label">MS_LOGIN</label>
        </div>
        <div class="form-group row">
            <label for="MS_PASSWORD" class="col-sm-2 col-form-label">Пароль</label>
            <div class="col-sm-3">
                <input id="MS_PASSWORD" name="MS_PASSWORD" value="<?=$options['MS_PASSWORD']?>" placeholder="MS_PASSWORD" class="form-control" />
            </div>
            <label for="MS_PASSWORD" class="col-sm-2 col-form-label">MS_PASSWORD</label>
        </div>
        <div class="form-group row">
            <div class="col-sm-10">
                <button type="submit" class="btn btn-primary">Сохранить</button>
            </div>
        </div>
    </form>
</div>
<!-- Optional JavaScript -->
<!-- jQuery first, then Popper.js, then Bootstrap JS -->
<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.6/umd/popper.min.js" integrity="sha384-wHAiFfRlMFy6i5SRaxvfOCifBUQy1xHdJ/yoi7FRNXMRBu5WHdZYu1hA6ZOblgut" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.2.1/js/bootstrap.min.js" integrity="sha384-B0UglyR+jN6CkvvICOB2joaf5I4l3gm9GU6Hc1og6Ls7i6U/mkkaduKaBhlAXv9k" crossorigin="anonymous"></script>
</body>
</html>
<?

	set_time_limit(0);
	ini_set('mysql.connect_timeout', 300);
	ini_set('default_socket_timeout', 300);
	
	error_reporting(E_ALL);
	ini_set('display_errors', 1);
	
	// Временная зона
	date_default_timezone_set ('Europe/Minsk');
	
	// Работа с файлами
	define('DS', DIRECTORY_SEPARATOR);
	define('BASE',  str_replace('core/', '',dirname(__FILE__).DS));

    define('OPTIONS_FILE', 'options'.DS.'options.json');

    generateConstants();

    // Из файла настроек генерируем константы
    function generateConstants() {
        $file = BASE.OPTIONS_FILE;
        if (!file_exists($file)) {
            return false;
        }
        $json = file_get_contents($file);
        if (empty($json)) {
            return false;
        }
        $arr = json_decode($json, true);
        if (empty($arr)) {
            return false;
        }
        foreach($arr as $k=>$v) {
            define($k, $v);
        }
    }
?>